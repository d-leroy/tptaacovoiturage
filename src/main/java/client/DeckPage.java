package client;

import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.VerticalPanel;



public class DeckPage extends VerticalPanel {

	public final static DeckPanel deckPanel = new DeckPanel();
		
	public DeckPage() {

		this.add(deckPanel);

		deckPanel.add(DeckPagesEnum.LIST_EVENEMENTS.getPanel());
		deckPanel.add(DeckPagesEnum.LIST_EQUIPAGES.getPanel());
		deckPanel.add(DeckPagesEnum.NEW_USER.getPanel());
		deckPanel.add(DeckPagesEnum.NEW_EVENT.getPanel());
		
	
		show(DeckPagesEnum.LIST_EVENEMENTS);
	}

	public static void show(DeckPagesEnum page) {
		deckPanel.showWidget(page.ordinal());
	}
}


