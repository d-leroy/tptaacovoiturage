package client;

import client.ui.Crews;
import client.ui.Events;
import client.ui.NewEvent;
import client.ui.NewUser;

import com.google.gwt.user.client.ui.VerticalPanel;

public enum DeckPagesEnum {

//	HOME(new Accueil()),
	LIST_EVENEMENTS(new Events()),
	LIST_EQUIPAGES(new Crews()),
 	NEW_USER(new NewUser()),
	NEW_EVENT(new NewEvent());
	
	
	private VerticalPanel panel;
	
	private DeckPagesEnum(VerticalPanel panel) {
		this.panel = panel;
	}
	
	public VerticalPanel getPanel() {
		return panel;
	}

}
