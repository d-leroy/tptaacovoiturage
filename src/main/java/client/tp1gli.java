package client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class tp1gli extends VerticalPanel implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	private VerticalPanel header = new VerticalPanel();
	private VerticalPanel content = new VerticalPanel();

	private Button eventsButton = new Button("voir les spectacles");
	private HTML welcome = new HTML("<H1>Bienvenue dans Go voiturage !</H1>");

	private Label warning = new Label("en cours de réalisation");

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		//		DECKPANEL.setHeight("900");
		//		DECKPANEL.setWidth("900");
		header.add(warning);

		content.add(welcome);
		content.add(eventsButton);

		RootPanel.get().add(header);
		RootPanel.get().add(content);

		eventsButton.addClickHandler(new ClickHandler() {

			@SuppressWarnings("static-access")
			public void onClick(ClickEvent arg0) {
				DeckPage deck = new DeckPage();
		
				RootPanel.get().clear();
				RootPanel.get().add(deck);

			}
		});

	}

}