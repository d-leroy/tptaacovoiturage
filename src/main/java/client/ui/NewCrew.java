package client.ui;

import java.util.List;

import shared.beans.CrewItf;
import shared.beans.EventItf;
import shared.beans.Location;
import shared.beans.LocationItf;
import shared.beans.LocationListItf;
import shared.beans.UserItf;
import client.DeckPage;
import client.DeckPagesEnum;
import client.jsonConverter.CrewJsonConverter;
import client.jsonConverter.EventJsonConverter;
import client.jsonConverter.LocationListJsonConverter;
import client.jsonConverter.UserJsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class NewCrew extends DialogBox {

	public NewCrew(boolean modal, final EventItf event) {

		super(false, modal);
		
		setText("S'inscrire en tant que conducteur");

		this.setSize("500px", "500px");

		VerticalPanel container = new VerticalPanel();
		VerticalPanel header = new VerticalPanel();
		VerticalPanel top = new VerticalPanel();
		VerticalPanel bottom = new VerticalPanel();

		container.setSize("100%", "100%");
		container.setSpacing(10);
		container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		header.setSpacing(10);
		top.setSpacing(10);
		bottom.setSpacing(10);

		Button closeButton = new Button("Close");

		closeButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {

				NewCrew.this.hide();
			}

		});

		Label loginLabel = new Label("Login ");
		final TextBox loginTextBox = new TextBox();
		Label aireLabel = new Label("Aire de Coivoiturage ");
		final ListBox aireListBox = getAireLBox();

		Button ValideButton = new Button("Valider");

		ValideButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				String aireCoVoiturage = aireListBox.getValue(aireListBox.getSelectedIndex());
				String login = loginTextBox.getText() ;
				validation(login, aireCoVoiturage, event);
			}
		});

		bottom.add(loginLabel);
		bottom.add(loginTextBox);
		bottom.add(aireLabel);
		bottom.add(aireListBox);
		bottom.add(ValideButton);

		header.add(closeButton);		

		container.add(header);
		container.add(top);
		container.add(bottom);

		setWidget(container);

	}

	private ListBox getAireLBox() {

		final ListBox lb = new ListBox();

		RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/location/locations/" );
		rb.setCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {

				if (response.getStatusCode() == 200) {

					String str = response.getText();
					LocationListItf locList = LocationListJsonConverter.getInstance().deserializeFromJson(str);
					List<LocationItf> list = locList.getLocations();
					for (LocationItf loc : list) {
						lb.addItem(loc.getDepartement() +" - "+loc.getCity()+" - "+loc.getName() + " - " +
								loc.getId()) ;
					}
				}
			}

			public void onError(Request arg0, Throwable arg1) {	}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}

		return lb;  

	}

	private void validation(String login, final String aireCoVoiturage, final EventItf event) {

		// récupération du user grâce à son login
		RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/user/login/" + login );
		rb.setCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {

				if (response.getStatusCode() == 200) {
					String str = response.getText();
					UserItf user = UserJsonConverter.getInstance().deserializeFromJson(str);
					if (user.getCar() != null ) {
						LocationItf loc = getLoc(aireCoVoiturage);
						creatingCrew(user, loc, event);
					} else {
					  Window.alert("Vous devez avoir une voiture pour vous inscrire");
					}
					
				} else {
					Window.alert("Login inconnu - Veuillez-vous enregister avant");
				}

			}

			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Erreur - dans la recherche de login");
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}

	private LocationItf getLoc(String aireCoVoiturage) {
		LocationItf loc = new Location();
		String[] strloc = aireCoVoiturage.split(" - ");
		loc.setDepartement(Integer.parseInt(strloc[0]));
		loc.setCity((strloc[1]));
		loc.setName(strloc[2]);
		loc.setId(Integer.parseInt(strloc[3]));
		return loc;
	}

	private void creatingCrew(UserItf user, LocationItf loc, final EventItf event ) {

		final CrewItf newCrew;
		
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, GWT
				.getHostPageBaseURL() + "rest/event/" + event.getId()+"/addcrew/");
		rb.setHeader("Content-Type", "application/json");
		newCrew = CrewJsonConverter.getInstance().makeCrew();
		newCrew.setDriver(user);
		newCrew.setLocation(loc);
		
		String newCrewToJson = CrewJsonConverter.getInstance().serializeToJson(newCrew);
		rb.setRequestData(newCrewToJson);

		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable exception) {
				Window.alert(exception.getMessage());
			}

			public void onResponseReceived(Request request,	Response response) {
				if (200 == response.getStatusCode() || 204 == response.getStatusCode()) {
					Window.alert("création event crew reussi");
					refresh(event.getId());
		
					}
				else {
					Window.alert("création event crew a échouée (" + response.getStatusCode() +") - Veuillez contacter le webMaster");
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * permet de réafficher la liste des crews 
	 */
	private void reafficherCrews(EventItf event) {
		Crews crewsList = (Crews) DeckPagesEnum.LIST_EQUIPAGES.getPanel();
		crewsList.clear();
	 	crewsList.initCrewsPanel(event);
		DeckPage.show(DeckPagesEnum.LIST_EQUIPAGES);
	}
	
	private void refresh(long id) { 
		
		final 
		
		RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/event/" + id );
		rb.setCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {

				if (response.getStatusCode() == 200) {
					String str = response.getText();
					EventItf event = EventJsonConverter.getInstance().deserializeFromJson(str);
					reafficherCrews(event);
					
				} else {
					Window.alert("Login inconnu - Veuillez-vous enregister avant");
				}

			}

			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Erreur - dans la recherche de login");
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}
}