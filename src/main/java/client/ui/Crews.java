package client.ui;

//import com.google.gwt.user.client.ui.Button;
import shared.beans.CrewItf;
import shared.beans.EventItf;
import shared.beans.UserItf;
import client.DeckPage;
import client.DeckPagesEnum;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class Crews extends VerticalPanel {

	// GWT
	private VerticalPanel content;
	private VerticalPanel top;
	private VerticalPanel center;

	private SectionStack sectionStack;


	public  Crews() {
		super();
	}

	public void initCrewsPanel(final EventItf event) {

		content = new VerticalPanel();
		top = new VerticalPanel();
		center  = new VerticalPanel();

		HorizontalPanel buttonsHP = new HorizontalPanel();
		HorizontalPanel titreHP = new HorizontalPanel();

		IButton eventsButton = new IButton("Voir Spectacles");
		eventsButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				DeckPage.show(DeckPagesEnum.LIST_EVENEMENTS);
			}});

		IButton newCrewButton = new IButton("Nouvel équipage");
		newCrewButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				NewCrew maFen = new NewCrew(false, event) ;
				maFen.show();
				maFen.center();
			}});

		IButton newUserButton = new IButton("Nouveau membre");
		newUserButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				DeckPage.show(DeckPagesEnum.NEW_USER);
			}});

		buttonsHP.add(eventsButton);
		buttonsHP.add(new HTML("<p>   </p>"));
		buttonsHP.add(newCrewButton);
		buttonsHP.add(new HTML("<p>   </p>"));
		buttonsHP.add(newUserButton);

		HTML titreHtml = new HTML(); 
		String titre = event.getTitle();
		String html= "<h1>Liste des équipages pour l'évènement : " + titre + "</h1>";
		titreHtml.setHTML(html);
		titreHP.add(titreHtml);

		top.add(buttonsHP);
		top.add(titreHP);

		if (event.getCrews().isEmpty()) {
			HTML message = new HTML("<h4> Il n'y a pas encore d'équipage</h4>");
			center.add(message);
		} else {
			sectionStack = new SectionStack();  
			sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
			sectionStack.setWidth(200);  
			sectionStack.setHeight(400);  
			affichage(event);
			center.add(sectionStack);
		}

		content.add(top);
		content.add(center);

		this.add(content);

	}

	private void affichage(final EventItf event) {

		for(int i=0; i<event.getCrews().size();i++) {

			final CrewItf crew = event.getCrews().get(i);

			SectionStackSection section = new SectionStackSection(""+crew.getLocation().getDepartement() + " - " + 
					crew.getLocation().getCity() + " - " + crew.getLocation().getName());

			section.setExpanded(false);
			section.setCanCollapse(true);

			HTMLFlow htmlFlowDriver = new HTMLFlow();
			htmlFlowDriver.setContents("Conducteur : " + crew.getDriver().getLogin()+"("+crew.getDriver().getNom());
			htmlFlowDriver.setHeight(30);

			int nb = crew.getDriver().getCar().getNbSeats() - crew.getPassengers().size();
			
			HTMLFlow htmlFlowSeat = new HTMLFlow();
			htmlFlowSeat.setContents("Place disponible : " + nb );
			htmlFlowSeat.setHeight(30);

			IButton passengerButton = new IButton("Voir Passagers");
			passengerButton.setHeight("20");
			passengerButton.setWidth("100");
			passengerButton.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent e) {

					if (crew.getPassengers().size() > 0) {
						String str = "";
						for (UserItf u : crew.getPassengers()) {
							str += "Passager : " + u.getLogin() + "(" + u.getNom() + ") \n" ;
						}
						Window.alert(str);
					} else {
						Window.alert("Pas encore de passager");
					}
				}});

			IButton registerButton = null;
			if (nb > 0) {
				registerButton = new IButton("S'incrire");
				registerButton.setHeight("20");
				registerButton.setWidth("100");
				registerButton.addClickHandler(new ClickHandler() {

					public void onClick(ClickEvent e) {
						registrationPassenger(crew, event);
					}});
			}
			VLayout vLayoutSection = new VLayout();
			vLayoutSection.setWidth(200);
			vLayoutSection.setHeight(60);

			vLayoutSection.addMember(htmlFlowDriver);
			vLayoutSection.addMember(htmlFlowSeat);
			vLayoutSection.addMember(passengerButton);
			if (nb > 0) {
				vLayoutSection.addMember(registerButton);
			}

			section.addItem(vLayoutSection);
			sectionStack.addSection(section);
		}
	}


	private void registrationPassenger(CrewItf crew, EventItf event) {
		AddingPassenger maFen = new AddingPassenger(false, crew, event);
		maFen.show();
		maFen.center();
	}

}

