package client.ui;

import shared.beans.Event;
import shared.beans.EventItf;
import client.DeckPage;
import client.DeckPagesEnum;
import client.jsonConverter.EventJsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class NewEvent extends VerticalPanel {  

	private VerticalPanel content;
	private VerticalPanel top;
	private VerticalPanel bottom;


	private HTML titreHtml = new HTML("<h1>Nouveau Spectacle</h1>");

	// Smart GWT

	public NewEvent() {  

		super();

		content = new VerticalPanel();
		top = new VerticalPanel();
		bottom  = new VerticalPanel();




		Label titleLabel = new Label("Titre ");
		final TextBox titleTextBox = new TextBox();
		Label placeLabel = new Label("Ville ");
		final TextBox placeTextBox = new TextBox();
		Label dayLabel = new Label("jour ");
		final TextBox dayTextBox = new TextBox();
		Label monthLabel = new Label("mois ");
		final TextBox monthTextBox = new TextBox();
		Label yearLabel = new Label("année ");
		final TextBox yearTextBox = new TextBox();
		Label hourLabel = new Label("heure ");
		final TextBox hourTextBox = new TextBox();
		Label minuteLabel = new Label("minute ");
		final TextBox minuteTextBox = new TextBox();
		Label espaceVide = new Label("--------------------------------");
		Button ValideButton = new Button("Valider");

		ValideButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				String[] data = new String[7]; 
				data[0] = titleTextBox.getText()   ;
				data[1] = placeTextBox.getText();
				data[2] = dayTextBox.getText();
				data[3] = monthTextBox.getText();
				data[4] = yearTextBox.getText();
				data[5] = hourTextBox.getText();
				data[6] = minuteTextBox.getText();
				if (!validationData(data)) {
					Window.alert("tous les champs sont obligatoires");
				}
				else if (!validationDateTime(data)) {
					Window.alert("Jour, mois, heure & minute doivent être numériques et à 2 chiffres \n" +
							"Année doit être numerique et à 4 chiffres \n" +
							"Jour compris entre 1 et 31 - Mois compris entre 1 et 12 \n" +
							"Heure comprise entre 00 et 23 - Minute comprise entre 00 et 59");
				} 
				else {
					creatingEvent(data);
				}
			}
		});

		bottom.add(titleLabel);
		bottom.add(titleTextBox);
		bottom.add(placeLabel);
		bottom.add(placeTextBox);
		bottom.add(dayLabel);
		bottom.add(dayTextBox);
		bottom.add(monthLabel);
		bottom.add(monthTextBox);
		bottom.add(yearLabel);
		bottom.add(yearTextBox);
		bottom.add(hourLabel);
		bottom.add(hourTextBox);
		bottom.add(minuteLabel);
		bottom.add(minuteTextBox);
		bottom.add(espaceVide);
		bottom.add(ValideButton);

		top.add(titreHtml);

		content.add(top);
		content.add(bottom);

		this.add(content);

	}

	private boolean validationData(String[] data){ 

		boolean validate = true;

		// les champs doivent être renseignés
		for (int i=0; i < data.length - 1; i++ ) {
			if (data[i].equals("") ) {
				validate = false;
			}
		}
		return validate;
	}

	private boolean validationDateTime(String[] data){ 

		boolean validate = true;

		if((!data[2].matches("[0-9][0-9]") && data[2].length()>0) 
				|| (!data[3].matches("[0-9][0-9]") && data[3].length()>0) 
				|| (!data[4].matches("[0-9][0-9][0-9][0-9]") && data[4].length()>0)
				|| (!data[5].matches("[0-9][0-9]") && data[5].length()>0)
				|| (!data[6].matches("[0-9][0-9]") && data[6].length()>0) ) 
		{
			validate = false;
		} else {
			if (Integer.parseInt(data[2]) > 31 
					|| 	Integer.parseInt(data[3]) > 12 
				    ||  Integer.parseInt(data[5]) > 23
				    ||  Integer.parseInt(data[6]) > 59)
				{
				validate = false;
				}
		}
		return validate;
	}


	private void creatingEvent(String[] data) {
		EventItf newEvent = new Event();

		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, GWT
				.getHostPageBaseURL() + "rest/event/create/");
		rb.setHeader("Content-Type", "application/json");
		newEvent = EventJsonConverter.getInstance().makeEvent();
		newEvent.setTitle(data[0]);
		newEvent.setPlace(data[1]);
		String date = data[2] + data[3] + data[4] ;
		newEvent.setDate(Integer.parseInt(date));
		String time = data[5] + data[6] ;
		newEvent.setHour(Integer.parseInt(time));
		String newEventToJson = EventJsonConverter.getInstance().serializeToJson(newEvent);
		rb.setRequestData(newEventToJson);

		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable exception) {
				Window.alert(exception.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode() || 204 == response.getStatusCode() ) {
					Window.alert("creation réussite");
					DeckPage.show(DeckPagesEnum.LIST_EVENEMENTS);
				}
				else {
					Window.alert("création a échouée (" + response.getStatusCode() +") - Veuillez contacter le webMaster");
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}




}