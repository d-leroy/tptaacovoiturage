package client.ui;

import java.util.List;

import shared.beans.EventItf;
import shared.beans.EventListItf;
import client.DeckPage;
import client.DeckPagesEnum;
import client.jsonConverter.EventJsonConverter;
import client.jsonConverter.EventListJsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class Events extends VerticalPanel{

	// GWT
	private VerticalPanel content;
	private VerticalPanel top;
	private VerticalPanel bottom;
	private HorizontalPanel header;
	
	private HTML titreHtml = new HTML("<h1>GOVOITURAGE</h1>");

	// Smart GWT
	final SectionStack sectionStack;

	public  Events() {
		super();

		content = new VerticalPanel();
		top = new VerticalPanel();
		bottom  = new VerticalPanel();
		header = new HorizontalPanel();

		IButton newUserButton = new IButton("Nouveau membre");
		newUserButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				DeckPage.show(DeckPagesEnum.NEW_USER);
			}});
		
		IButton newEventButton = new IButton("Nouveau spectacle");
		newEventButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				DeckPage.show(DeckPagesEnum.NEW_EVENT);
			}});

		// SMARTGWT
		sectionStack = new SectionStack();  
		sectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);  
		sectionStack.setWidth(200);  
		sectionStack.setHeight(400);  

		RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/event/events" );
		rb.setCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {

				if (response.getStatusCode() == 200) {
					String str = response.getText();
					EventListItf eventList = EventListJsonConverter.getInstance().deserializeFromJson(str);
					List<EventItf> events = eventList.getEvents();
					affichage(events);
				}
			}

			public void onError(Request arg0, Throwable arg1) {

			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}

		header.add(newUserButton);
		header.add(new HTML("<p>   </p>"));
		header.add(newEventButton);
		
		top.add(header);
		top.add(titreHtml);
		bottom.add(sectionStack);

		content.add(top);
		content.add(bottom);

		this.add(content);

	}

	/**
	 * Affichage des données de chaque évènement
	 * 
	 * @param events
	 */
	private void affichage(List<EventItf> events) {

		for(int i=0; i<events.size();i++) {

			SectionStackSection section = new SectionStackSection(""+events.get(i).getTitle());  
			section.setExpanded(false);
			section.setCanCollapse(true);

			HTMLFlow htmlFlowLocation = new HTMLFlow();
			htmlFlowLocation.setContents(""+events.get(i).getPlace());
			htmlFlowLocation.setHeight(30);

			HTMLFlow htmlFlowDate = new HTMLFlow();
			String dateStr = String.valueOf(events.get(i).getDate());
			String dateF = dateStr.substring(0,2) +"/"+dateStr.substring(2,4)+"/"+	dateStr.substring(4,8);
			String hourStr = String.valueOf(events.get(i).getHour());
			String hourF = hourStr.substring(0,2) +"h"+hourStr.substring(2,4);

			htmlFlowDate.setContents("Le "+ dateF + " à " + hourF );
			htmlFlowDate.setHeight(30);

			IButton signUpButton = new IButton("Voir Participants");
			signUpButton.setHeight("20");
			signUpButton.setWidth("100");
			final EventItf event = events.get(i);


			signUpButton.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent e) {
					refresh(event.getId());
				}});

			VLayout vLayoutSection = new VLayout();
			vLayoutSection.setWidth(200);
			vLayoutSection.setHeight(60);

			vLayoutSection.addMember(htmlFlowLocation);
			vLayoutSection.addMember(htmlFlowDate);
			vLayoutSection.addMember(signUpButton);

			section.addItem(vLayoutSection);
			sectionStack.addSection(section);
		}

	}

	/**
	 * Permet de mettre à jour l'evenement dans le cas ou un equipage a été ajouté sur la page des crews.
	 * @param id
	 */

	private void refresh(long id) { 

		RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/event/" + id );
		rb.setCallback(new RequestCallback() {

			public void onResponseReceived(Request request, Response response) {

				if (response.getStatusCode() == 200) {
					String str = response.getText();
					EventItf event = EventJsonConverter.getInstance().deserializeFromJson(str);
					reafficherCrews(event);

				} else {
					Window.alert("Login inconnu - Veuillez-vous enregister avant");
				}

			}

			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Erreur - dans la recherche de login");
			}

		});

		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}

	/**
	 * permet de réafficher la liste des crews 
	 */
	private void reafficherCrews(EventItf event) {
		Crews crewsList = (Crews) DeckPagesEnum.LIST_EQUIPAGES.getPanel();
		crewsList.clear();
	 	crewsList.initCrewsPanel(event);
		DeckPage.show(DeckPagesEnum.LIST_EQUIPAGES);
	}
}
