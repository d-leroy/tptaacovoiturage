package client.ui;

import shared.beans.Car;
import shared.beans.CarItf;
import shared.beans.User;
import shared.beans.UserItf;
import client.DeckPage;
import client.DeckPagesEnum;
import client.jsonConverter.CarJsonConverter;
import client.jsonConverter.UserJsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class NewUser extends VerticalPanel {  

	private VerticalPanel content;
	private VerticalPanel top;
	private VerticalPanel bottom;


	private HTML titreHtml = new HTML("<h1>Inscription</h1>");

	// Smart GWT

	public NewUser() {  

		super();

		content = new VerticalPanel();
		top = new VerticalPanel();
		bottom  = new VerticalPanel();

		IButton eventsButton = new IButton("Voir Spectacles");
		eventsButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
				DeckPage.show(DeckPagesEnum.LIST_EVENEMENTS);
			}});

		affichageFormulaire();

		top.add(eventsButton);
		top.add(titreHtml);

		content.add(top);
		content.add(bottom);

		this.add(content);

	}

	private void affichageFormulaire() {

		VerticalPanel form = new VerticalPanel();

		HTML loginHtml = new HTML("Login*");
		final TextBox loginTBox = new TextBox();
		HTML nameHtml = new HTML("Nom*");
		final TextBox nameTBox = new TextBox();
		HTML lastNameHtml = new HTML("Prenom*");
		final TextBox lastNameTBox = new TextBox();
		Label espaceVide = new Label("--------------------------------");
		HTML DriverHtml = new HTML("Conducteur");
		final ListBox driverListBox = new ListBox();
		driverListBox.addItem("NON");
		driverListBox.addItem("OUI");
		HTML immatHtml = new HTML("Immatriculation");
		final TextBox immatTBox = new TextBox();
		HTML nbPlaceHtml = new HTML("Place(s) disponible(s)");
		final ListBox nbPlaceListBox = new ListBox();
		nbPlaceListBox.addItem("1");
		nbPlaceListBox.addItem("2");
		nbPlaceListBox.addItem("3");
		nbPlaceListBox.addItem("4");
		nbPlaceListBox.addItem("5");
		nbPlaceListBox.addItem("6");
		nbPlaceListBox.addItem("7");
		nbPlaceListBox.addItem("8");

		Label espaceVide1 = new Label("--------------------------------");

		IButton validateButton = new IButton("Valider");
		validateButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {

				String[] data = new String[5];

				if (loginTBox.getText().equals("") ||
						nameTBox.getText().equals("")  || 
						lastNameTBox.getText().equals("")) {
					Window.alert("Login, nom et prenom doivent être renseignés");
				}
				else {
					if (driverListBox.getValue(driverListBox.getSelectedIndex()).equals("OUI")) {
						if (immatTBox.getText().equals("")) {
							Window.alert("L'immatriculation de la voiture doit être renseignée");
						} 
						else {
							data[0] = loginTBox.getText();	
							data[1] = nameTBox.getText();
							data[2] = lastNameTBox.getText();
							if (nbPlaceListBox.getSelectedIndex() > 0) {
								data[3] = nbPlaceListBox.getValue(nbPlaceListBox.getSelectedIndex());
							}
							else {
								data[3] = "1";
							}
							data[4] = immatTBox.getText(); 
							creatingCar(data);
						}

					}
					else { 
						data[0] = loginTBox.getText();	
						data[1] = nameTBox.getText();
						data[2] = lastNameTBox.getText();
						creatingUser(data, false);
					}
				}
			}});

		form.add(loginHtml);
		form.add(loginTBox);
		form.add(nameHtml);
		form.add(nameTBox);
		form.add(lastNameHtml);
		form.add(lastNameTBox);
		form.add(espaceVide);
		form.add(DriverHtml);
		form.add(driverListBox);
		form.add(immatHtml);
		form.add(immatTBox);
		form.add(nbPlaceHtml);
		form.add(nbPlaceListBox);
		form.add(espaceVide1);
		form.add(validateButton);

		bottom.add(form);

	}

	private void creatingCar(final String[] data){
		CarItf newCar = new Car();

		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, GWT
				.getHostPageBaseURL() + "rest/car/create");
		rb.setHeader("Content-Type", "application/json");
		newCar = CarJsonConverter.getInstance().makeCar();
		newCar.setNbSeats(Integer.parseInt(data[3]));
		newCar.setImat(data[4]);
		String newCarToJson = CarJsonConverter.getInstance().serializeToJson(newCar);
		rb.setRequestData(newCarToJson);
		
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable exception) {
				Window.alert(exception.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode() || 204 == response.getStatusCode() ) {
					creatingUser(data, true);
				}
				else {
					Window.alert("création a échouée (" + response.getStatusCode() +") - Veuillez contacter le webMaster");
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}
	}

	private void creatingUser(String[] data, boolean driver) {
		UserItf newUser = new User();
		Car  newCar;  
		if (driver) {
			newCar = new Car();
			newCar.setNbSeats(Integer.parseInt(data[3]));
			newCar.setImat(data[4]);
		} else {
			newCar = null ;
		}
		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, GWT
				.getHostPageBaseURL() + "rest/user/create");
		rb.setHeader("Content-Type", "application/json");
		newUser = UserJsonConverter.getInstance().makeUser();
		newUser.setLogin(data[0]);
		newUser.setNom(data[1]);
		newUser.setPrenom(data[2]);
		newUser.setCar(newCar);
		String newUserToJson = UserJsonConverter.getInstance().serializeToJson(newUser);
		rb.setRequestData(newUserToJson);
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable exception) {
				Window.alert(exception.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode() || 204 == response.getStatusCode()) {
					Window.alert("Félicitation vous êtes inscrit");
				}
				else {
					Window.alert("création a échouée (" + response.getStatusCode() +") - Veuillez contacter le webMaster");
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}

	}

}