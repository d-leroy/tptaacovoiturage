package client.ui;

import shared.beans.CrewItf;
import shared.beans.EventItf;
import shared.beans.UserItf;
import client.DeckPage;
import client.DeckPagesEnum;
import client.jsonConverter.UserJsonConverter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class AddingPassenger extends DialogBox {

	private CrewItf crew;
	private EventItf event;

	public AddingPassenger(boolean modal, CrewItf crew, EventItf event) {

		super(false, modal);
		this.crew = crew ;
		this.event = event ;

		setText("S'inscrire en tant que passager");

		this.setSize("500px", "500px");

		VerticalPanel container = new VerticalPanel();
		VerticalPanel header = new VerticalPanel();
		VerticalPanel top = new VerticalPanel();
		VerticalPanel bottom = new VerticalPanel();

		container.setSize("100%", "100%");
		container.setSpacing(10);
		container.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		header.setSpacing(10);
		top.setSpacing(10);
		bottom.setSpacing(10);

		header.setHeight("50");
		top.setHeight("150");
		bottom.setHeight("300");

		Button closeButton = new Button("Close");

		closeButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {

				AddingPassenger.this.hide();
			}

		});

		final Label loginLabel = new Label("Login");
		final TextBox loginTextBox = new TextBox();

		Button validationButton = new Button("Valider");

		validationButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				validation(loginTextBox.getText());
			}

		});

		header.add(closeButton);		
		bottom.add(loginLabel);
		bottom.add(loginTextBox);
		bottom.add(validationButton);

		container.add(header);
		container.add(top);
		container.add(bottom);

		setWidget(container);

	}

	private void validation(String login) {

		if (trouverLogin(login)) {
			Window.alert("L'utilisateur " + login + " est déjà inscrit à cet équipage");
		}
		else {
			RequestBuilder rb = new RequestBuilder (RequestBuilder.GET, GWT.getHostPageBaseURL() + "rest/user/login/" + login );
			rb.setCallback(new RequestCallback() {

				public void onResponseReceived(Request request, Response response) {

					if (response.getStatusCode() == 200) {
						String str = response.getText();
						UserItf user = UserJsonConverter.getInstance().deserializeFromJson(str);
						updateCrew(user);

					} else {
						Window.alert("Login inconnu - Veuillez-vous enregister avant");
					}

				}

				public void onError(Request arg0, Throwable arg1) {
					Window.alert("Erreur - dans la recherche de login");
				}

			});

			try {
				rb.send();
			} catch (RequestException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean trouverLogin(String login) {
		boolean trouver = false;
				
		for (UserItf u : this.crew.getPassengers()) {
			if (u.getLogin().equals(login)) {
				trouver = true;
			}
		}

		return trouver;

	}

	/**
	 * Ajout d'un user dans la liste des passagers de l'équipage
	 * Appel du service pour la mise à jour de Crew dans la base.
	 * @param user
	 */
	private void updateCrew(final UserItf user) {

		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, GWT
				.getHostPageBaseURL() + "rest/crew/"+crew.getId()+"/adduser/"+user.getId());
		rb.setHeader("Content-Type", "application/json");
	 	rb.setRequestData("");
			
		rb.setCallback(new RequestCallback() {
			
			public void onError(Request arg0, Throwable arg1) {
				Window.alert("Probleme inscription - Veuillez contact le webmaster");
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode() || 204 == response.getStatusCode() ) {
					String str = "Inscription reussi \n" + 
				                 "Pour revenir à la liste des equipages \n" +
							     "cliquer sur le bouton close" ;
					Window.alert(str);
					crew.getPassengers().add(user);
					reafficherCrews();
				} else {
					Window.alert("Ajout user dans passangers echoué : " + response.getStatusCode());
				}

			}

		});
		
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
		}

	}

	/**
	 * permet de réafficher la liste des crews avec le nombre de places restant mis à jour
	 */
	private void reafficherCrews() {
		Crews crewsList = (Crews) DeckPagesEnum.LIST_EQUIPAGES.getPanel();
		crewsList.clear();
		crewsList.initCrewsPanel(event);
		DeckPage.show(DeckPagesEnum.LIST_EQUIPAGES);
	}
}
