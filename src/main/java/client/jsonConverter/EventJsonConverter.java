package client.jsonConverter;

import shared.beans.EventItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class EventJsonConverter {

	private EventJsonConverter() {}

	private static EventJsonConverter instance = new EventJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public EventItf makeEvent() {
		// Construct the AutoBean
		AutoBean<EventItf> eventList = factory.event();
		// Return the Event interface shim
		return eventList.as();
	}

	public String serializeToJson(EventItf event) {
		// Retrieve the AutoBean controller
		AutoBean<EventItf> bean = AutoBeanUtils.getAutoBean(event);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static EventJsonConverter getInstance() {
		return instance;
	}
	
	public EventItf deserializeFromJson(String json) {
		AutoBean<EventItf> bean = AutoBeanCodex.decode(factory, EventItf.class, json);
		return bean.as();
	}
	
}
