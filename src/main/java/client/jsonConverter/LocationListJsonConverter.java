package client.jsonConverter;

import shared.beans.LocationListItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class LocationListJsonConverter {

	private LocationListJsonConverter() {}

	private static LocationListJsonConverter instance = new LocationListJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public LocationListItf makeLocationList() {
		// Construct the AutoBean
		AutoBean<LocationListItf> locationList = factory.locationList();
		// Return the location interface shim
		return locationList.as();
	}

	public String serializeToJson(LocationListItf locationList) {
		// Retrieve the AutoBean controller
		AutoBean<LocationListItf> bean = AutoBeanUtils.getAutoBean(locationList);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static LocationListJsonConverter getInstance() {
		return instance;
	}
	
	public LocationListItf deserializeFromJson(String json) {
		AutoBean<LocationListItf> bean = AutoBeanCodex.decode(factory, LocationListItf.class, "{\"locations\":" + json + "}");
		return bean.as();
	}
	
}
