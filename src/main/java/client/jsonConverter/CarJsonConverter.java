package client.jsonConverter;

import shared.beans.CarItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class CarJsonConverter {

	private CarJsonConverter() {}

	private static CarJsonConverter instance = new CarJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public CarItf makeCar() {
		// Construct the AutoBean
		AutoBean<CarItf> carList = factory.car();
		// Return the Event interface shim
		return carList.as();
	}

	public String serializeToJson(CarItf car) {
		// Retrieve the AutoBean controller
		AutoBean<CarItf> bean = AutoBeanUtils.getAutoBean(car);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static CarJsonConverter getInstance() {
		return instance;
	}
	
	public CarItf deserializeFromJson(String json) {
		AutoBean<CarItf> bean = AutoBeanCodex.decode(factory, CarItf.class, json);
		return bean.as();
	}
	
}
