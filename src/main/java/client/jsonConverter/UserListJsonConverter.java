package client.jsonConverter;

import shared.beans.UserListItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class UserListJsonConverter {

	private UserListJsonConverter() {}

	private static UserListJsonConverter instance = new UserListJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public UserListItf makeUserList() {
		// Construct the AutoBean
		AutoBean<UserListItf> userList = factory.userList();
		// Return the Event interface shim
		return userList.as();
	}

	public String serializeToJson(UserListItf userList) {
		// Retrieve the AutoBean controller
		AutoBean<UserListItf> bean = AutoBeanUtils.getAutoBean(userList);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static UserListJsonConverter getInstance() {
		return instance;
	}
	
	public UserListItf deserializeFromJson(String json) {
		AutoBean<UserListItf> bean = AutoBeanCodex.decode(factory, UserListItf.class, "{\"users\":" + json + "}");
		return bean.as();
	}
	
}
