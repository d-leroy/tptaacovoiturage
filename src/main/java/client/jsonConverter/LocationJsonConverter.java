package client.jsonConverter;

import shared.beans.LocationItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class LocationJsonConverter {

	private LocationJsonConverter() {}

	private static LocationJsonConverter instance = new LocationJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public LocationItf makeLocation() {
		// Construct the AutoBean
		AutoBean<LocationItf> locationList = factory.location();
		// Return the Event interface shim
		return locationList.as();
	}

	public String serializeToJson(LocationItf location) {
		// Retrieve the AutoBean controller
		AutoBean<LocationItf> bean = AutoBeanUtils.getAutoBean(location);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static LocationJsonConverter getInstance() {
		return instance;
	}
	
	public LocationItf deserializeFromJson(String json) {
		AutoBean<LocationItf> bean = AutoBeanCodex.decode(factory, LocationItf.class, json);
		return bean.as();
	}
	
}
