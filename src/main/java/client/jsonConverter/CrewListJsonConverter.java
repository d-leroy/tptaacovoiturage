package client.jsonConverter;

import shared.beans.CrewListItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;


public class CrewListJsonConverter {

	private CrewListJsonConverter() {}

	private static CrewListJsonConverter instance = new CrewListJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public CrewListItf makeCrewList() {
		// Construct the AutoBean
		AutoBean<CrewListItf> crewList = factory.crewList();
		// Return the Event interface shim
		return crewList.as();
	}

	public String serializeToJson(CrewListItf crewList) {
		// Retrieve the AutoBean controller
		AutoBean<CrewListItf> bean = AutoBeanUtils.getAutoBean(crewList);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static CrewListJsonConverter getInstance() {
		return instance;
	}
	
	public CrewListItf deserializeFromJson(String json) {
		AutoBean<CrewListItf> bean = AutoBeanCodex.decode(factory, CrewListItf.class, "{\"crews\":" + json + "}");
		return bean.as();
	}
	
}
