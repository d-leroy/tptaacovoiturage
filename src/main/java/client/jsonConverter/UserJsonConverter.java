package client.jsonConverter;

import shared.beans.UserItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class UserJsonConverter {

	private UserJsonConverter() {}

	private static UserJsonConverter instance = new UserJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public UserItf makeUser() {
		// Construct the AutoBean
		AutoBean<UserItf> userList = factory.user();
		// Return the Event interface shim
		return userList.as();
	}

	public String serializeToJson(UserItf user) {
		// Retrieve the AutoBean controller
		AutoBean<UserItf> bean = AutoBeanUtils.getAutoBean(user);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static UserJsonConverter getInstance() {
		return instance;
	}
	
	public UserItf deserializeFromJson(String json) {
		AutoBean<UserItf> bean = AutoBeanCodex.decode(factory, UserItf.class, json);
		return bean.as();
	}
	
}
