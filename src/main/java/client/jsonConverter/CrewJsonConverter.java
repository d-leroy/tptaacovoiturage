package client.jsonConverter;

import shared.beans.CrewItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class CrewJsonConverter {

	private CrewJsonConverter() {}

	private static CrewJsonConverter instance = new CrewJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public CrewItf makeCrew() {
		// Construct the AutoBean
		AutoBean<CrewItf> crewList = factory.crew();
		// Return the Event interface shim
		return crewList.as();
	}

	public String serializeToJson(CrewItf crew) {
		// Retrieve the AutoBean controller
		AutoBean<CrewItf> bean = AutoBeanUtils.getAutoBean(crew);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static CrewJsonConverter getInstance() {
		return instance;
	}
	
	public CrewItf deserializeFromJson(String json) {
		AutoBean<CrewItf> bean = AutoBeanCodex.decode(factory, CrewItf.class, json);
		return bean.as();
	}
	
}
