package client.jsonConverter;

import shared.beans.EventListItf;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

public class EventListJsonConverter {

	private EventListJsonConverter() {}

	private static EventListJsonConverter instance = new EventListJsonConverter();

	// Instantiate the factory
	shared.Factory factory = GWT.create(shared.Factory.class);
	// In non-GWT code, use AutoBeanFactorySource.create(MyFactory.class);

	public EventListItf makeEventList() {
		// Construct the AutoBean
		AutoBean<EventListItf> eventList = factory.eventList();
		// Return the Event interface shim
		return eventList.as();
	}

	public String serializeToJson(EventListItf eventList) {
		// Retrieve the AutoBean controller
		AutoBean<EventListItf> bean = AutoBeanUtils.getAutoBean(eventList);
		return AutoBeanCodex.encode(bean).getPayload();
	}

	public static EventListJsonConverter getInstance() {
		return instance;
	}
	
	public EventListItf deserializeFromJson(String json) {
		AutoBean<EventListItf> bean = AutoBeanCodex.decode(factory, EventListItf.class, "{\"events\":" + json + "}");
		return bean.as();
	}
	
}
