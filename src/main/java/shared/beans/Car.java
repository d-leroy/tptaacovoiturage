package shared.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Car implements Serializable, CarItf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String imat;
	private int nbSeats;
	
	public Car(){}
	
	@Id
	public String getImat() {
		return imat;
	}
	public void setImat(String imat) {
		this.imat = imat;
	}
	
	public int getNbSeats() {
		return nbSeats;
	}
	public void setNbSeats(int nbSeats) {
		this.nbSeats = nbSeats;
	}
	
}
