package shared.beans;

import java.util.List;

public interface EventListItf {
	void setEvents(List<EventItf> listEvents);
	List<EventItf> getEvents();
}
