package shared.beans;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as = User.class)
public interface UserItf {

	public long getId();
	public void setId(long id);

	public String getLogin();
	public void setLogin(String login);

	public String getNom();
	public void setNom(String nom);

	public String getPrenom();
	public void setPrenom(String prenom);

	public CarItf getCar();
	public void setCar(CarItf car);

	
}