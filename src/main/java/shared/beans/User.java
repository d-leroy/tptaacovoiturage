package shared.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User implements Serializable, UserItf {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;

	private String login;
	private String nom;
	private String prenom;
	
	private CarItf car;
	
	public User(){}
	
	@GeneratedValue
	@Id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
			
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	@OneToOne(cascade={CascadeType.REMOVE}, targetEntity = Car.class)
	public CarItf getCar() {
		return car;
	}
	
	public void setCar(CarItf car) {
		this.car = car;
	}
	
}
