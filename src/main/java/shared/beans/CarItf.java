package shared.beans;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as = Car.class)
public interface CarItf {

	public String getImat();

	public void setImat(String imat);

	public int getNbSeats();

	public void setNbSeats(int nbSeats);

}