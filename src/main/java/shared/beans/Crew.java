package shared.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Crew implements Serializable, CrewItf {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private UserItf driver;
	private List<UserItf> passengers;
 	private LocationItf location; // aire de co-voiturage

	public Crew() {}; 
	
	@GeneratedValue
	@Id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@OneToOne(targetEntity = User.class)
	public UserItf getDriver() {
		return driver;
	}
	public void setDriver(UserItf driver) {
		this.driver = driver;
	}
	
	@ManyToMany(targetEntity = User.class)
	public List<UserItf> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<UserItf> passengers) {
		this.passengers = passengers;
	}
	
	@ManyToOne(targetEntity = Location.class)
	public LocationItf getLocation() {
		return location;
	}
	public void setLocation(LocationItf location) {
		this.location = location;
	}
	
}