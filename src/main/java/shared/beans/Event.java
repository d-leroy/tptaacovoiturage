package shared.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Event implements Serializable, EventItf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;	
	
	private String title;
	private String place;
	private int date;
	private int hour;

	private List<CrewItf> crews;
	
	public Event() {};
	
	@GeneratedValue	
	@Id	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String titre) {
		this.title = titre;
	}
	
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	
	@OneToMany(targetEntity = Crew.class)
	//@OneToMany(targetEntity = Crew.class)
	public List<CrewItf> getCrews() {
		return crews;
	}
	public void setCrews(List<CrewItf> crews) {
		this.crews = crews;
	}

}