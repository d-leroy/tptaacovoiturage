package shared.beans;

import java.util.List;

public interface LocationListItf {
	void setLocations(List<LocationItf> listLocations);
	List<LocationItf> getLocations();
	

}
