package shared.beans;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as = Location.class)
public interface LocationItf {

	public long getId();

	public void setId(long id);

	public String getName();

	public void setName(String name);

	public String getCity();

	public void setCity(String city);

	public int getDepartement();

	public void setDepartement(int departement);

}