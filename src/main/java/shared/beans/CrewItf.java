package shared.beans;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as = Crew.class)
public interface CrewItf {

	public long getId();

	public void setId(long id);
	
	public UserItf getDriver();

	public void setDriver(UserItf driver);

	public List<UserItf> getPassengers();

	public void setPassengers(List<UserItf> passengers);

	public LocationItf getLocation();

	public void setLocation(LocationItf location);

}