package shared.beans;

import java.util.List;

public interface CrewListItf {
	void setCrews(List<CrewItf> listCrews);
	List<CrewItf> getCrews();
}
