package shared.beans;

import java.util.List;

public interface EventItf {

	public long getId();

	public void setId(long id);

	public String getTitle();

	public void setTitle(String titre);

	public int getDate();

	public void setDate(int date);

	public String getPlace();

	public void setPlace(String place);

	public int getHour();

	public void setHour(int hour);

	public List<CrewItf> getCrews();

	public void setCrews(List<CrewItf> crews);

}