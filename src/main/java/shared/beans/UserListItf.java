package shared.beans;

import java.util.List;

public interface UserListItf {
	void setUsers(List<UserItf> listUsers);
	List<UserItf> getUsers();
}
