package shared;

import shared.beans.CarItf;
import shared.beans.CrewItf;
import shared.beans.CrewListItf;
import shared.beans.EventItf;
import shared.beans.EventListItf;
import shared.beans.LocationItf;
import shared.beans.LocationListItf;
import shared.beans.UserItf;
import shared.beans.UserListItf;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;

public interface Factory extends AutoBeanFactory{
	AutoBean<EventItf> event();
	AutoBean<EventListItf> eventList();
	AutoBean<LocationItf> location();
	AutoBean<LocationListItf> locationList();
	AutoBean<CrewItf> crew();
	AutoBean<CrewListItf> crewList();
	AutoBean<UserItf> user();
	AutoBean<UserListItf> userList();
	AutoBean<CarItf> car();
}
