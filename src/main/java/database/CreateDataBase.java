package database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import shared.beans.Car;
import shared.beans.Crew;
import shared.beans.CrewItf;
import shared.beans.Event;
import shared.beans.Location;
import shared.beans.User;
import shared.beans.UserItf;

public class CreateDataBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("dev");
		EntityManager manager = factory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		
		try {
			Location loc1 = new Location();
			loc1.setName("La Boissiere");
			loc1.setCity("Concarneau");
			loc1.setDepartement(29);
			Location loc2 = new Location();
			loc2.setName("Troyalac'h"); 
			loc2.setCity("Quimper");
			loc2.setDepartement(29);
			Location loc3 = new Location();
			loc3.setName("Penhoat Salaün"); 
			loc3.setCity("Pleuven");
			loc3.setDepartement(29);
			Location loc4 = new Location();
			loc4.setName("Les 4 routes");
			loc4.setCity("Breal sous Montfort");
			loc4.setDepartement(35);
			Location loc5 = new Location();
			loc5.setName("La Louviere"); 
			loc5.setCity("Bruz");
			loc5.setDepartement(35);
			Location loc6 = new Location();
			loc6.setName("Vaux"); 
			loc6.setCity("Rennes");
			loc6.setDepartement(35);
			
			User usr1 =  new User();
			usr1.setLogin("user1");
			usr1.setNom("ZOLA");
			usr1.setPrenom("Emile");
			User usr2 = new User();
			usr2.setLogin("user2");
			usr2.setNom("De BALZAC");
			usr2.setPrenom("Honore");
			User usr3 = new User();
			usr3.setLogin("user3");
			usr3.setNom("HUGO");
			usr3.setPrenom("Victor");
			User usr4 = new User();
			usr4.setLogin("user4");
			usr4.setNom("COCTEAU");
			usr4.setPrenom("Jean");
			User usr5 = new User();
			usr5.setLogin("user5");
			usr5.setNom("VERLAINE");
			usr5.setPrenom("Paul");
			User usr6 =  new User();
			usr6.setLogin("user6");
			usr6.setNom("SARTRE");
			usr6.setPrenom("Jean-Paul");
			User usr7 = new User();
			usr7.setLogin("user7");
			usr7.setNom("DAUDET");
			usr7.setPrenom("Alphonse");
			User usr8 = new User();
			usr8.setLogin("user8");
			usr8.setNom("PAGNOL");
			usr8.setPrenom("Marcel");
			User usr9 = new User();
			usr9.setLogin("user9");
			usr9.setNom("De MUSSET");
			usr9.setPrenom("Alfred");
			User usr10 = new User();
			usr10.setLogin("user10");
			usr10.setNom("APOLLINAIRE");
			usr10.setPrenom("Guillaume");
			
			Event evt1 = new Event();
			evt1.setTitle("Le roi soleil");
			evt1.setPlace("Rennes");
			evt1.setDate(21122014);
			evt1.setHour(2030);
			
			Event evt2 = new Event();
			evt2.setTitle("Les vieilles charues");
			evt2.setPlace("Carhaix");
			evt2.setDate(15072015);
			evt2.setHour(2030);
			
			Event evt3 = new Event();
			evt3.setTitle("Folle journée");
			evt3.setPlace("Nantes");
			evt3.setDate(28012015);
			evt3.setHour(1000);
			
			Event evt4 = new Event();
			evt4.setTitle("Dirty Dancing");
			evt4.setPlace("Quimper");
			evt4.setDate(28012015);
			evt4.setHour(2030);
			
			Event evt5 = new Event();
			evt5.setTitle("Flasdance");
			evt5.setPlace("Angers");
			evt5.setDate(2805015);
			evt5.setHour(2030);
			
			manager.persist(usr1);
			manager.persist(usr2);
			manager.persist(usr3);
			manager.persist(usr4);
			manager.persist(usr5);
			manager.persist(usr6);
			manager.persist(usr7);
			manager.persist(usr8);
			manager.persist(usr9);
			manager.persist(usr10);
			manager.persist(loc1);
			manager.persist(loc2);
			manager.persist(loc3);
			manager.persist(loc4);
			manager.persist(loc5);
			manager.persist(loc6);
			manager.persist(evt1);
			manager.persist(evt2);
			manager.persist(evt3);
			manager.persist(evt4);
			manager.persist(evt5);
			
			Car car1  = new Car();
			car1.setImat("aa 642 aa");
			car1.setNbSeats(3);
			manager.persist(car1);
			usr1.setCar(car1);
			Car car2  = new Car();
			car2.setImat("bb 22 bb");
			car2.setNbSeats(4);
			manager.persist(car2);
			usr2.setCar(car2);
			Car car3  = new Car();
			car3.setImat("cc 45 cc");
			car3.setNbSeats(2);
			manager.persist(car3);
			usr3.setCar(car3);
			
			Crew crew1 = new Crew();
			manager.persist(crew1);
			crew1.setLocation(loc1);
 			crew1.setDriver(usr1);
			List<UserItf> passengers = new ArrayList<UserItf>();
			passengers.add(usr6);
			passengers.add(usr7);
			crew1.setPassengers(passengers);
				
			Crew crew2 = new Crew();
			manager.persist(crew2);
			crew2.setLocation(loc2);
 			crew2.setDriver(usr2);
			List<UserItf> passengers1 = new ArrayList<UserItf>();
			passengers1.add(usr7);
			passengers1.add(usr8);
			crew2.setPassengers(passengers1);
			
			Crew crew3 = new Crew();
			manager.persist(crew3);
			crew3.setLocation(loc3);
 			crew3.setDriver(usr1);
			List<UserItf> passengers2 = new ArrayList<UserItf>();
			passengers2.add(usr9);
			crew3.setPassengers(passengers2);
			
			List<CrewItf> crews = new ArrayList<CrewItf>();
			crews.add(crew1);
 		 	crews.add(crew2);
			evt1.setCrews(crews);
			
			List<CrewItf> crews1 = new ArrayList<CrewItf>();
			crews1.add(crew3);
			evt2.setCrews(crews1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		
	}
}
