package database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerImpl {

	private static EntityManager manager;

	public static EntityManager getEntityManager() {

		if (manager == null) {
			EntityManagerFactory factory = Persistence
					.createEntityManagerFactory("dev");
			manager  = factory.createEntityManager();
		}

		return manager;
	}

}
