package server.services.implementations;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import server.services.interfaces.LocationManagerInterface;
import shared.beans.Location;
import database.EntityManagerImpl;

@Path("location")
public class LocationManagerImpl implements LocationManagerInterface{

	@PUT
	@Path("create/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(Location loc) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(loc);
		et.commit();
	}

	@DELETE
	@Path("delete/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Location delete(@PathParam("id") String id) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Location loc = em.find(Location.class, Integer.parseInt(id));
		em.remove(loc);
		et.commit();
	
		return loc;
		
	}

	@POST
	@Path("update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Location loc) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.merge(loc);
		et.commit();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("locations/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Location> getLocationList() {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.createQuery("select l from Location as l order by l.departement, l.city, l.name").getResultList();
	}


}