package server.services.implementations;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import server.services.interfaces.UserManagerInterface;
import shared.beans.User;
import database.EntityManagerImpl;

@Path("user")
public class UserManagerImpl implements UserManagerInterface{

	@PUT
	@Path("create/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(User user) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(user);
		et.commit();
	}

	public void delete(User user) {
		//		EntityManager em = EntityManagerImpl.getEntityManager();
		//		em.getTransaction().begin();
		//		em.remove(user);
		//		em.getTransaction().commit();

	}

	public void update(User user) {
		//		EntityManager em = EntityManagerImpl.getEntityManager();
		//		em.getTransaction().begin();
		//		EntityManagerImpl.getEntityManager().merge(user);
		//		em.getTransaction().commit();

	}

	@GET
	@Path("id/{id_user}")
	@Produces(MediaType.APPLICATION_JSON)
	public User findUserById(@PathParam("id_user") String userId) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.find(User.class, Long.parseLong(userId));
	}

	@GET
	@Path("login/{login}")
	@Produces(MediaType.APPLICATION_JSON)
	public User findUserByLogin(@PathParam("login") String login) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		String str = "select u from User as u where u.login = :login" ;
		Query query = em.createQuery(str);
		query.setParameter("login", login);
		return (User) query.getResultList().get(0);
	}

}
