package server.services.implementations;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import database.EntityManagerImpl;
import server.services.interfaces.CarManagerInterface;
import shared.beans.Car;
import shared.beans.CarItf;

@Path("car")
public class CarManagerImpl implements CarManagerInterface {

	@PUT
	@Path("create/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(Car car) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(car);
		et.commit();
	}

	@DELETE
	@Path("cardelete/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public CarItf delete(@PathParam("id") String imat) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		CarItf car = em.find(Car.class, imat);
		em.remove(car);
		et.commit();
	
		return car;

	}

	@POST
	@Path("carupdate/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Car car) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.merge(car);
		et.commit();

	}

	
}
