package server.services.implementations;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import server.services.interfaces.CrewManagerInterface;
import shared.beans.Crew;
import shared.beans.User;
import shared.beans.UserItf;
import database.EntityManagerImpl;

@Path("crew")
public class CrewManagerImpl implements CrewManagerInterface{

	@PUT
	@Path("create/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(Crew crew) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(crew);		
		et.commit();
	}

	@DELETE
	@Path("delete/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Crew delete(@PathParam("id") String id) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Crew crew = em.find(Crew.class, Integer.parseInt(id));
		em.remove(crew);
		et.commit();
		return crew;
	}

	@POST
	@Path("update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Crew crew) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.merge(crew);
		et.commit();
	}

	@POST
	@Path("{id_crew}/adduser/{id_user}")
	//	@Consumes(MediaType.APPLICATION_JSON)
	public void addUser(@PathParam("id_crew") String crewId, @PathParam("id_user") String userId) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Crew crew = em.find(Crew.class, Long.parseLong(crewId));
		UserItf user = em.find(User.class, Long.parseLong(userId));
		List<UserItf> list = crew.getPassengers();
		list.add(user);
		crew.setPassengers(list);
		em.merge(crew);
		et.commit();
	}

	//ok
	@GET
	@Path("crewById/{id_crew}")
	@Produces(MediaType.APPLICATION_JSON)
	public Crew findCrewById(@PathParam("id_crew") String crewId) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.find(Crew.class, Long.parseLong(crewId));
	}

	@GET
	@Path("crews/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Crew> getCrewList() {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.createQuery("select e from Crew as e").getResultList();
	}


	//	@SuppressWarnings("unchecked")
	@GET
	@Path("event/{id_event}/crews/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Crew> getCrewListByIdevent(@PathParam("id_event") String eventId) {
		Long id = Long.valueOf(eventId).longValue();
		EntityManager em = EntityManagerImpl.getEntityManager();
		String str = "select c from Event as e " +
				"join e.crews as c " +
				"where e.id = :idEvent" ;
		Query query = em.createQuery(str);
		query.setParameter("idEvent", id);
		return query.getResultList();
	}
	// http://localhost:8080/rest/crew/event/1/crews/


}
