package server.services.implementations;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import server.services.interfaces.EventManagerInterface;
import shared.beans.Crew;
import shared.beans.CrewItf;
import shared.beans.Event;
import database.EntityManagerImpl;

@Path("event")
public class EventManagerImpl implements EventManagerInterface {

	@PUT
	@Path("create/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(Event event) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(event);
		et.commit();
	}
	
	@POST
	@Path("update/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Event event) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.merge(event);
		et.commit();
	}
	
	@POST
	@Path("{id_event}/addcrew/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addCrew(Crew crew, @PathParam("id_event") String eventId) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Event event = em.find(Event.class, Long.parseLong(eventId));
		System.out.println("id event :" + event.getId());
		Crew newCrew = new Crew();
		newCrew.setDriver(crew.getDriver());
		newCrew.setLocation(crew.getLocation());
		em.persist(newCrew);
		List<CrewItf> list = event.getCrews();
		list.add(newCrew);
		event.setCrews(list);
		em.merge(event);
		et.commit();
	}


	@DELETE
	@Path("delete/{id}")	
	@Produces({MediaType.APPLICATION_JSON})
	public Event delete(@PathParam("id") String id) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		EntityTransaction et = em.getTransaction();
		et.begin();
		Event event = em.find(Event.class, Integer.parseInt(id));
		em.remove(event);
		et.commit();

		return event;
	}
	
	//ok
	@GET
	@Path("{id_event}")
	@Produces(MediaType.APPLICATION_JSON)
	public Event findEventById(@PathParam("id_event") String eventId) {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.find(Event.class, Long.parseLong(eventId));
	}

	//ok
	@SuppressWarnings("unchecked")
	@GET
	@Path("events/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Event> getEventList() {
		EntityManager em = EntityManagerImpl.getEntityManager();
		return em.createQuery("select e from Event as e").getResultList();
	}

	//ok
	@SuppressWarnings("unchecked")
	@GET
	@Path("{id_event}/crews/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Crew> getCrewList(@PathParam("id_event") String eventId) {
		Long id = Long.valueOf(eventId).longValue();
		EntityManager em = EntityManagerImpl.getEntityManager();
		String str = "select c from Event as e " +
		             "join e.crews as c " +
				     "where e.id = :idEvent" ;
		Query query = em.createQuery(str);
		query.setParameter("idEvent", id);
		return query.getResultList();
	}
	

}