package server.services.interfaces;

import java.util.Collection;

import shared.beans.Crew;

public interface CrewManagerInterface {

	public void create(Crew crew);

	public Crew delete(String id);

	public void update(Crew crew);

	public Collection<Crew> getCrewList();
	/**
	 * Récupération de la liste des équipages pour un évènement donné.
	 * @param idEvent
	 * @return
	 */
	public Collection<Crew> getCrewListByIdevent(String idEvent);
	
}
