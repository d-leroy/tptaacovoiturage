package server.services.interfaces;

import java.util.Collection;

import shared.beans.Location;

public interface LocationManagerInterface {

public void create(Location loc);
	
	public Location delete(String id);
	
	public void update(Location loc);

	public Collection<Location> getLocationList(); 
		
}
