package server.services.interfaces;

import shared.beans.Car;
import shared.beans.CarItf;

public interface CarManagerInterface {
	
	public void create(Car car);
	
	public CarItf delete(String imat);
	
	public void update(Car car);
	
}
