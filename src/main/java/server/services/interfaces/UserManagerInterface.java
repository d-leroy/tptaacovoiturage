package server.services.interfaces;

import shared.beans.User;

public interface UserManagerInterface {
	
	public void create(User user);
	
	public void delete(User user);
	
	public void update(User user);
	
	public User findUserById(String id);
	
	public User findUserByLogin(String login);
	
}
