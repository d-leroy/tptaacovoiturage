package server.services.interfaces;

import java.util.Collection;

import shared.beans.Event;

public interface EventManagerInterface {

	public void create(Event event);

	public Event delete(String id);
	
	public Event findEventById(String id);
	
	public Collection<Event> getEventList();
	
//	public Collection<Crew> getCrewList(String id);
	
//	public Collection<User> getDriverList(String id);
//
//	public Collection<User> getPassengerList(String id);
//	
//	public void addDriver(String eventId, String driverId);
//
//	public void addPassenger(String eventId, String passengerId);

}
