'use strict';

/**
 * Définition des contrôleurs
 */
var myAppControllers = angular.module('myAppControllers', []);
 
//Contrôleur de la page des évènements
myAppControllers.controller('eventsCtrl', ['$scope', '$http', 
                                           function($scope, $http){
	console.log("logging my app");
	$http({
		method : 'GET',
		url : 'rest/event/events/'
	}).success(function(data) {
		//console.log(data);
		$scope.events = data;
	});
}
]);
 
//Contrôleur de la page de contact
myAppControllers.controller('crewCtrl', ['$scope','$http', 
                                         function($scope, $http){
	$http({
		method : 'GET',
		url : 'rest/event/{id_event}/crews/'
	}).success(function(data) {
		//console.log(data);
		$scope.crews = data;
	});
}
]);
