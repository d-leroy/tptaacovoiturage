'use strict';
 

/**
 * Déclaration de l'application routeApp
 */
var myApp = angular.module('myApp', [
                                     'ngRoute',
                                     'ui.bootstrap',
                                     'myAppControllers',
                                     ]);
 
/**
 * Configuration du module principal : myApp
 */
myApp.config(['$routeProvider',
              function($routeProvider) { 
 
	$routeProvider
	.when('/events', {
		templateUrl: 'events.html',
		controller: 'eventsCtrl'
	})
//	.when('/crews', {
//		templateUrl: 'crews.html',
//		controller: 'crewsCtrl'
//	})
	.otherwise({
		redirectTo: 'index.html'
	});
}
]);
 
